import { defineConfig } from 'umi';

export const config = defineConfig({
  layout: {
    name: '锐哥的中台',
    locale: true,
    layout: 'side',
  },
  nodeModulesTransform: {
    type: 'none',
  },
  routes: [{ path: '/', component: '@/pages/index' }],
  fastRefresh: {},
});
